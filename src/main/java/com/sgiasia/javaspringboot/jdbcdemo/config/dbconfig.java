package com.sgiasia.javaspringboot.jdbcdemo.config;

public class dbconfig
{

    public static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    public static final String URL         = "jdbc:mysql://localhost:3306/jdbcdemo?useLegacyDatetimeCode=false&serverTimezone=UTC";
    public static final String USERNAME    = "spring";
    public static final String PASSWORD    = "123";

}
